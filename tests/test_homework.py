import random
import string

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

administrator_email = 'administrator@testarena.pl'


class LoginPage:

    def __init__(self, browser):
        self.browser = browser

    def visit(self):
        self.browser.get('http://demo.testarena.pl/zaloguj')

    def login(self, email, password):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
        self.browser.find_element(By.CSS_SELECTOR, '#login').click()


class AdminPanelPage:

    def __init__(self, browser):
        self.browser = browser

    def open_admin_panel(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin a').click()


class ProjectsPage:

    def __init__(self, browser):
        self.browser = browser

    def add_new_project(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, '.button_link').click()
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()

    def search_added_project(self, search_term):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(search_term)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def verify_added_project(self, search_term):
        found_projects = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
        assert len(found_projects) > 0

        names = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr td:nth-of-type(1)')
        for name in names:
            assert search_term.lower() in name.text.lower()


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    yield browser

    browser.quit()


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


def test_testarena(browser):
    # zalogowanie się do TestArena
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email

    # otwarcie panelu administratora
    admin_panel_page = AdminPanelPage(browser)
    admin_panel_page.open_admin_panel()

    # dodanie nowego projektu o losowej nazwie i losowym prefixe
    project_name = get_random_string(10)
    prefix = get_random_string(6)
    projects_page = ProjectsPage(browser)
    projects_page.add_new_project(project_name)

    # przejście do sekcji Projekty
    browser.find_element(By.CSS_SELECTOR, '.item2').click()

    # wyszukanie nowo utworzonego projektu i weryfikacja
    projects_page.search_added_project(project_name)
    projects_page.verify_added_project(project_name)
